package negocio;

import java.math.BigDecimal;

public class Vehiculo implements Comparable<Vehiculo>{

	private String marca;
	private String modelo;
	private BigDecimal precio;
	
	Vehiculo(String marca, String modelo, BigDecimal precio){
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
	}

	@Override
	public int compareTo(Vehiculo arg0) {
		
		return arg0.precio.compareTo(this.precio);
	}
	
	public String getMarca() {
		return marca;
	}

	public String getModelo() {
		return modelo;
	}

	public BigDecimal getPrecio() {
		return precio;
	}

	
	
}
