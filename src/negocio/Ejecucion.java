package negocio;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Ejecucion {

	public static void main(String[] args) {
		
		//Cargamos los vehiculos	
		Concesionaria concesionaria = new Concesionaria();
		
		Auto auto0 = new Auto("Peugeot", "206", new BigDecimal(200000.00), 4);
		Auto auto1 = new Auto("Peugeot", "208", new BigDecimal(250000.00), 5);
		Moto moto0 = new Moto("Honda", "Titan", new BigDecimal(60000.00), "125cc");
		Moto moto1 = new Moto("Yamaha", "YBR", new BigDecimal(80500.50), "160cc");
		
		concesionaria.cargarVehiculo(auto0);
		concesionaria.cargarVehiculo(moto0);
		concesionaria.cargarVehiculo(auto1);
		concesionaria.cargarVehiculo(moto1);
		
		//Preparamos el formato para mostrar los precios
		DecimalFormat formateador = new DecimalFormat("$###,##0.00");
		
		//Mostramos todos los vehiculos con detalle
		concesionaria.listarVehiculos();
		System.out.println("=======================================");
		
		//Mostramos el vehiculo m�s caro
		System.out.println("Veh�culo m�s caro: "+""+concesionaria.masCaro().getMarca()+" "+concesionaria.masCaro().getModelo()+" ");
		
		//Mostramos el vehiculo m�s barato
		System.out.println("Veh�culo m�s barato: "+""+concesionaria.masBarato().getMarca()+" "+concesionaria.masBarato().getModelo()+" ");

		//Mostramos el vehiculo que tiene "Y" en el modelo
		System.out.println("Veh�culo que contiene en el modelo la letra �Y�:"+" "+concesionaria.modeloContieneLetra("Y").getMarca()+" "+concesionaria.modeloContieneLetra("Y").getModelo()+" "+formateador.format(concesionaria.modeloContieneLetra("Y").getPrecio()));
		System.out.println("=======================================");
		//Listamos los vehiculos de mayor a menor segun precio
		concesionaria.listarVehiculosDescendente();


	}

}
