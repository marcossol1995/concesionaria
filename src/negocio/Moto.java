package negocio;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Moto extends Vehiculo {
	private String cilindrada;

	Moto(String marca, String modelo, BigDecimal precio, String cilindrada) {
		super(marca, modelo, precio);
		this.cilindrada = cilindrada;
	}
	
	DecimalFormat formateador = new DecimalFormat("$###,##0.00");
	
	@Override
    public String toString() { 
        return String.format("Marca: "+this.getMarca() +" // "+"Modelo: "+this.getModelo()+" // "+"Cilindrada: "+this.cilindrada+" // "+"Precio: "+formateador.format(this.getPrecio())); 
    } 

}
