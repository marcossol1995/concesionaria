package negocio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

public class Concesionaria {
	private ArrayList<Vehiculo> vehiculos; 
	
	Concesionaria(){
		this.vehiculos = new ArrayList();
	}
	
	public void cargarVehiculo(Vehiculo vehiculo) {
		this.vehiculos.add(vehiculo);
	}
	
	public Vehiculo masCaro() {
		
		BigDecimal precioMasAlto = new BigDecimal(0);
		Vehiculo vehiculoMasCaro = new Vehiculo("", "", new BigDecimal(0));
		
		for(Vehiculo vehiculo : this.vehiculos) {
			if(vehiculo.getPrecio().compareTo(precioMasAlto)==1) {
				precioMasAlto = vehiculo.getPrecio();
				vehiculoMasCaro = vehiculo;
			}
		}
		
		return vehiculoMasCaro;
		
	}
	
	public Vehiculo masBarato() {
		
		BigDecimal precioMasBajo = new BigDecimal(0);
		Vehiculo vehiculoMasBarato = new Vehiculo("", "", new BigDecimal(0));
		
		for(Vehiculo vehiculo : this.vehiculos) {
			if(precioMasBajo.equals(new BigDecimal(0))) {
				precioMasBajo = vehiculo.getPrecio();
				vehiculoMasBarato = vehiculo;
			}
			if(vehiculo.getPrecio().compareTo(precioMasBajo)==-1) {
				precioMasBajo = vehiculo.getPrecio();
				vehiculoMasBarato = vehiculo;
			}
		}
		
		return vehiculoMasBarato;
		
	}
	
	public Vehiculo modeloContieneLetra(String l) {
		
		for(Vehiculo vehiculo : this.vehiculos) {
			if(vehiculo.getModelo().contains(l)) {
				return vehiculo;
			}
		}
		return null;
	}
	
	public void listarVehiculosDescendente() {
		ArrayList<Vehiculo> vehiculosReturn = this.vehiculos;
		Collections.sort(vehiculosReturn);
		
		System.out.println("VehÝculos ordenados por precio de mayor a menor:");
		for(Vehiculo vehiculo : vehiculosReturn) {
			System.out.print(vehiculo.getMarca()+" ");
			System.out.println(vehiculo.getModelo());
		}
	}
	
	public void listarVehiculos() {
		for(Vehiculo vehiculo : this.vehiculos) {
			System.out.println(vehiculo);
		}
	}
	
}
