package negocio;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Auto extends Vehiculo{
	private int cantPuertas;

	Auto(String marca, String modelo, BigDecimal precio, int cantPuertas) {
		super(marca, modelo, precio);
		this.cantPuertas = cantPuertas;
		
	}
	
	DecimalFormat formateador = new DecimalFormat("$###,##0.00");
	
	@Override
    public String toString() { 
        return String.format("Marca: "+this.getMarca() +" // "+"Modelo: "+this.getModelo()+" // "+"Puertas: "+this.cantPuertas+" // "+"Precio: "+formateador.format(this.getPrecio())); 
    } 

}
